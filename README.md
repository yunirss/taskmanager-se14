**Task Manager**

`Описание проекта`

> TASK MANAGER - web приложение по управлению данными о пользователях, проектах и задачах. С помощью REST API реализованы HTTP запросы для получения и сохранения данных в СУБД PostgreSQL. Сохранение и получение данных производится в формате JSON. В приложении используется Spring MVC. Репозитории реализованы на основе Spring Boot.

`Требования к SOFTWARE`
- MacOs
- Windows
- Linux

`Стек технологий`
- IntelliJ IDEA Ultimate
- Java SE Development 8
- Apache Maven version 4.0.0
- Jackson (Serialization)
- MVC(repository, service, ServiceLocator)
- Git
- MD5
- PostgreSQL
- Spring Boot
- Postman

`Разработчик`
- Суюндуков Юнир Ильфатович
- yunir14@yandex.ru

**REST запросы**

`User GET запросы:`
- Find all - /user/get
- Find by id - /user/get/{id}
- Find by login - /user/getByName/{login}

`Project GET запросы:`
- Find all - /project/get
- Find by id - /project/getById/{id}
- Find by name - /project/getByName/{name}
- Find by user id - /project/getProjectsByUserId/{user_id}

`Task GET запросы:`
- Find all - /task/get
- Find by id - /task/getById/{id}
- Find by name - /task/getByName/{name}
- Find by project id - /task/getTasksByProjectId/{project_id}
- Find by user id - /task/getTasksByUserId/{user_id}

`POST запросы:`
- Save user - /user/post
- Save project - /project/post
- Save task - /task/post

`PUT запросы:`
- Update user - /user/put
- Update project - /project/put
- Update task - /task/put

`DELETE запросы:`
- Delete user by id - /user/deleteById/{id}
- Delete project by id - /project/deleteById/{id}
- Delete task by id - /task/deleteById/{id}
- Delete task by name - /task/deleteByName/{name}
- Delete task by project id - /task/deleteProjectTasks/{project_id}
