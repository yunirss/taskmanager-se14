package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.suyundukov.tm.enums.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Класс, для работы с сущностью проекта
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "project")
public class Project implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.")
    @Column(name = "createdate", nullable = false)
    private LocalDateTime createDate;
    @Column(name = "userid", nullable = false)
    private int userId;
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    public Project(Integer id, String name, String description, LocalDateTime createDate, int userId, Status status) {
        this.id = id;
        this.name = name;
        this.createDate = createDate;
        this.description = description;
        this.userId = userId;
        this.status = status;
    }

    public Project() {

    }
}