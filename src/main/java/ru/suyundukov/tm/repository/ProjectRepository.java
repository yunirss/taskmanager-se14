package ru.suyundukov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.suyundukov.tm.entity.Project;

import java.util.List;
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Project findProjectById(Integer id);

    Project findProjectByName(String name) throws Exception;

    List<Project> findProjectsByUserId(int userId);

    void deleteProjectByName(String name) throws Exception;
    void deleteProjectById(Integer id) ;
}
