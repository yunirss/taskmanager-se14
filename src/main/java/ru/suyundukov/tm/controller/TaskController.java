package ru.suyundukov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.exception_handling.NoSuchEntityException;
import ru.suyundukov.tm.service.TaskService;

import java.util.List;

@RestController
@RequestMapping(value = "/task")
public class TaskController {
    private final TaskService service;

    @Autowired
    public TaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> save(@RequestBody Task task) throws Exception {
        service.save(task);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> update(@RequestBody Task task) throws Exception {
        service.save(task);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Task>> findAll() throws Exception {
        List<Task> tasks = service.findAll();
        if (tasks.isEmpty()) {
            throw new NoSuchEntityException("There is no tasks");
        }
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> findTaskById(@PathVariable Integer id) throws Exception {
        Task task = service.findById(id);
        if (task == null) {
            throw new NoSuchEntityException("There is no task with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @GetMapping(value = "/getTasksByUserId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Task>> findTasksByUserId(@PathVariable Integer id) throws Exception {
        List<Task> tasks = service.findTasksByUserId(id);
        if (tasks.isEmpty()) {
            throw new NoSuchEntityException("There is no tasks");
        }
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @GetMapping(value = "/getByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> findTaskByName(@PathVariable String name) throws Exception {
        Task task = service.findByName(name);
        if (task == null) {
            throw new NoSuchEntityException("There is no task with NAME = " + name + " in Database!");
        }
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @GetMapping(value = "/getTasksByProjectId/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Task>> findTasksByProjectId(@PathVariable Integer projectId) throws Exception {
        List<Task> tasks = service.findTasksByProjectId(projectId);
        if (tasks.isEmpty()) {
            throw new NoSuchEntityException("There is no tasks");
        }
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        service.deleteById(id);
        return "Task with Id = " + id + " was deleted";
    }

    @DeleteMapping(value = "/deleteByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteByName(@PathVariable String name) throws Exception {
        service.deleteByName(name);
        return "Task with NAME = " + name + " was deleted";
    }

    @DeleteMapping(value = "/deleteTasksByProjectId/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteTasksByProjectId(@PathVariable Integer projectId) throws Exception {
        service.deleteTasksByProjectId(projectId);
        return "Tasks with ProjectId = " + projectId + " was deleted";
    }
}
