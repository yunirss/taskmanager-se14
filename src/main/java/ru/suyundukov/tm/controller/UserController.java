package ru.suyundukov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.exception_handling.NoSuchEntityException;
import ru.suyundukov.tm.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> save(@RequestBody User user) throws Exception {
        service.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> update(@RequestBody User user) throws Exception {
        service.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> findAll() throws Exception {
        List<User> users = service.findAll();
        if (users.isEmpty()) {
            throw new NoSuchEntityException("There is no users");
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> findUserById(@PathVariable Integer id) throws Exception {
        User user = service.findById(id);
        if (user == null) {
            throw new NoSuchEntityException("There is no user with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "/getByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> findUserByName(@PathVariable String name) throws Exception {
        User user = service.findByName(name);
        if (user == null) {
            throw new NoSuchEntityException("There is no user with NAME = " + name + " in Database!");
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String delete(@PathVariable Integer id) throws Exception {
        service.deleteById(id);
        return "User with Id = " + id + " was deleted";
    }
}
