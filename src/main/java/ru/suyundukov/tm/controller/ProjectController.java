package ru.suyundukov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.exception_handling.NoSuchEntityException;
import ru.suyundukov.tm.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping(value = "/project")
public class ProjectController {
    private final ProjectService service;

    @Autowired
    public ProjectController(ProjectService service) {
        this.service = service;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> save(@RequestBody Project project) throws Exception {
        service.save(project);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> update(@RequestBody Project project) throws Exception {
        service.save(project);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Project>> findAll() throws Exception {
        List<Project> projects = service.findAll();
        if (projects.isEmpty()) {
            throw new NoSuchEntityException("There is no projects");
        }
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> findProjectById(@PathVariable Integer id) throws Exception {
        Project project = service.findById(id);
        if (project == null) {
            throw new NoSuchEntityException("There is no project with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(value = "/getByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> findProjectByName(@PathVariable String name) throws Exception {
        Project project = service.findByName(name);
        if (project == null) {
            throw new NoSuchEntityException("There is no project with NAME = " + name + " in Database!");
        }
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(value = "getProjectsByUserId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Project>> findProjectsByUserId(@PathVariable Integer id) {
        List<Project> projects = service.findProjectsByUserId(id);
        if (projects.isEmpty()) {
            throw new NoSuchEntityException("There is no projects");
        }
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        service.deleteById(id);
        return "Project with Id = " + id + " was deleted";
    }

    @DeleteMapping(value = "/deleteByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteByName(@PathVariable String name) throws Exception {
        service.deleteByName(name);
        return "Project with NAME = " + name + " was deleted";
    }
}
