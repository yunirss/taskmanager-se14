package ru.suyundukov.tm.service;

import ru.suyundukov.tm.entity.User;

public interface UserService extends AbstractService<User> {
}
