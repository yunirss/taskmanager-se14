package ru.suyundukov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public void save(Task task) {
        taskRepository.save(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();

    }

    @Override
    public Task findById(Integer id) {
        return taskRepository.findTaskById(id);
    }

    @Override
    public Task findByName(String name) {
        return taskRepository.findTaskByName(name);
    }

    public List<Task> findTasksByProjectId(int projectId) {
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public List<Task> findTasksByUserId(int userId) {
        return taskRepository.findTasksByUserId(userId);
    }

    public void deleteTasksByProjectId(int id) {
        taskRepository.deleteTasksByProjectId(id);
    }

    @Override
    public void deleteByName(String name) {
        taskRepository.deleteTaskByName(name);
    }

    @Override
    public void deleteById(Integer id) {
        taskRepository.deleteTaskById(id);
    }
}