package ru.suyundukov.tm.service;

import ru.suyundukov.tm.entity.Task;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface TaskService extends AbstractService<Task> {
    void deleteByName(String name) throws Exception;

    void deleteTasksByProjectId(int id) throws Exception;

    List<Task> findTasksByProjectId(int projectId) throws SQLException, IOException;

    List<Task> findTasksByUserId(int userId) throws SQLException, IOException;
}
