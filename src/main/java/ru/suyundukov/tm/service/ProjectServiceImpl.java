package ru.suyundukov.tm.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.repository.ProjectRepository;
import java.util.List;
@AllArgsConstructor
@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public void save(Project project) {
        projectRepository.save(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findById(Integer id) {
        return projectRepository.findProjectById(id);
    }

    @Override
    public Project findByName(String name) throws Exception {
        return projectRepository.findProjectByName(name);
    }

    public void deleteByName(String name) throws Exception {
        projectRepository.deleteProjectByName(name);
    }

    @Override
    public List<Project> findProjectsByUserId(int userId) {
        return projectRepository.findProjectsByUserId(userId);
    }

    @Override
    public void deleteById(Integer id) {
        projectRepository.deleteProjectById(id);
    }
}
