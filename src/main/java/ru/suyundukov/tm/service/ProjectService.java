package ru.suyundukov.tm.service;

import ru.suyundukov.tm.entity.Project;

import java.util.List;

public interface ProjectService extends AbstractService<Project> {
    void deleteByName(String name) throws Exception;
    List<Project> findProjectsByUserId(int userId);
}
